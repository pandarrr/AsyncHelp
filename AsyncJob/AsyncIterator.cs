﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace AsyncHelp
{
    /// <summary>
    /// 异步迭代器，用于处理需要在迭代过程中同时进行回调操作的情况
    /// </summary>
    public class AsyncIterator
    {
        private ConcurrentQueue<AsyncJob.CallBackInfo> _queue;
        private Func<object, AsyncIterator, object> _function;
        private object _state;

        /// <summary>
        /// 缓冲区长度，当迭代中发送的回调信号溢出缓冲区时，迭代将会阻塞并等待信号进入缓冲区后再继续执行
        /// </summary>
        public int BufferLength { get; set; }

        /// <summary>
        /// 生成异步迭代器的实例
        /// </summary>
        /// <param name="iterator">迭代方法</param>
        /// <param name="state">要传入迭代方法的参数</param>
        public AsyncIterator(Func<object, AsyncIterator, object> iterator, object state)
        {
            this._queue = new ConcurrentQueue<AsyncJob.CallBackInfo>();
            this._function = iterator;
            this.BufferLength = 10;
            this._state = state;
        }

        /// <summary>
        /// 向主线程发送回调信号，主线程将按序执行所有的回调方法
        /// </summary>
        /// <param name="act">回调方法</param>
        /// <param name="param">向回调方法传入的参数</param>
        public void SendCallback(Action<object> act, object param)
        {
            AsyncJob.CallBackInfo cb = new AsyncJob.CallBackInfo(act, param, 0);
            while (this._queue.Count >= this.BufferLength) { };
            this._queue.Enqueue(cb);
        }

        /// <summary>
        /// 开始执行迭代器及其回调方法
        /// </summary>
        /// <returns>迭代器返回的结果</returns>
        public object RunToCompletion()
        {
            Task<object> task = new Task<object>(() => this._function.Invoke(this._state, this));
            task.Start();
            while(task.Status != TaskStatus.Canceled && task.Status != TaskStatus.Faulted && task.Status != TaskStatus.RanToCompletion)
            {
                AsyncJob.CallBackInfo cb;
                if(this._queue.TryDequeue(out cb))
                {
                    cb.Invoke();
                }
            }
            if(this._queue.Count > 0)
            {
                foreach(AsyncJob.CallBackInfo cb in this._queue)
                {
                    cb.Invoke();
                }
            }
            return task.Result;
        }       
    }
}
