﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Collections;

namespace AsyncHelp
{
    /// <summary>
    /// 用于执行多线程异步操作的类
    /// </summary>
    public class AsyncJob
    {
        private List<Task> _tasks;
        private ConcurrentQueue<CallBackInfo> _queue;
        
        /// <summary>
        /// 异步操作的方法列表
        /// </summary>
        public List<FuncInfo> Methods { get; set; }

        /// <summary>
        /// 生成异步操作对象
        /// </summary>
        /// <param name="function">异步操作的方法</param>
        /// <param name="callBack">操作完成后的回调方法</param>
        /// <param name="state">为异步操作传入的参数</param>
        public AsyncJob(Func<object, object> function, Action<object> callBack, object state)
        {
            this._tasks = new List<Task>();
            this._queue = new ConcurrentQueue<CallBackInfo>();
            this.Methods = new List<FuncInfo>();
            this.Methods.Add(new FuncInfo() { Function = function, CallBack = callBack, State = state });
        }

        /// <summary>
        /// 开始执行所有的异步任务
        /// </summary>
        public void Start()
        {
            foreach (FuncInfo f in Methods)            {
                int taskId = 0;
                Action<object> action = param =>
                {
                    object result = f.Function(param);
                    this._queue.Enqueue(new CallBackInfo(f.CallBack, result, taskId));
                };
                Task task = new Task(action, f.State);
                taskId = task.Id;
                this._tasks.Add(task);
            }
            foreach (Task t in this._tasks)
            {              
                t.Start();                
            }
        }

        /// <summary>
        /// 等待并执行回调方法
        /// </summary>
        public void CallBack()
        {
            while(this._tasks.Count() > 0)
            {
                CallBackInfo cb;
                if (this._queue.TryDequeue(out cb))
                {
                    cb.Invoke();
                    Task task = this._tasks.Find(t => t.Id == cb.CurrentTask);
                    this._tasks.Remove(task);
                }
            }
        }

        /// <summary>
        /// 合并另一个异步任务
        /// </summary>
        /// <param name="another">另一个异步任务</param>
        /// <returns>合并后的异步任务</returns>
        public AsyncJob Con(AsyncJob another)
        {
            this.Methods.AddRange(another.Methods);
            return this;
        }

        /// <summary>
        /// 将多个异步任务合并为一个
        /// </summary>
        /// <param name="jobs">要合并的异步任务</param>
        /// <returns>合并后的异步任务</returns>
        public static AsyncJob Merge(params AsyncJob[] jobs)
        {
            if(jobs.Length < 1)
            {
                return new AsyncJob(o => null, o => { }, null);
            }
            if(jobs.Length == 1)
            {
                return jobs[0];
            }
            for(int i = 1; i < jobs.Length; i++)
            {
                jobs[0].Con(jobs[i]);
            }
            return jobs[0];
        }
        
        /// <summary>
        /// 异步任务的回调信息
        /// </summary>                    
        public class CallBackInfo
        {
            /// <summary>
            /// 回调方法
            /// </summary>
            public Action<object> Action { get; set; }

            /// <summary>
            /// 要传入回调方法的参数
            /// </summary>
            public object Param { get; set; }

            /// <summary>
            /// 发送该回调方法的任务ID
            /// </summary>
            public int CurrentTask { get; set; }

            /// <summary>
            /// 生成异步任务的回调信息的实例
            /// </summary>
            /// <param name="action">回调方法</param>
            /// <param name="param">要传入回调方法的参数</param>
            /// <param name="task">发送该回调信息的任务ID</param>
            public  CallBackInfo(Action<object> action, object param, int task)
            {
                this.Action = action;
                this.Param = param;
                this.CurrentTask = task;
            }

            /// <summary>
            /// 执行回调方法
            /// </summary>
            public void Invoke()
            {
                this.Action.Invoke(this.Param);
            }
        }

        /// <summary>
        /// 异步任务中要执行的方法信息
        /// </summary>
        public class FuncInfo
        {
            /// <summary>
            /// 异步任务中要执行的方法
            /// </summary>
            public Func<object, object> Function { get; set; }

            /// <summary>
            /// 方法完成后的回调方法
            /// </summary>
            public Action<object> CallBack { get; set; }

            /// <summary>
            /// 要传入方法的参数
            /// </summary>
            public object State { get; set; }
        }
    }

    public class AsyncEvent : INode
    {
        private ConcurrentQueue<Action> _queue;
        private Action<AsyncEvent> _func;
        private Task _task;
        private Circle<AsyncEvent> _circle;

        public INode Last { get; set; }
        public INode Next { get; set; }

        public int BufferLength { get; set; }

        public bool IsCompleted
        {
            get
            {
                if(this._task != null && this._task.Status != TaskStatus.Canceled && this._task.Status != TaskStatus.Faulted 
                    && this._task.Status != TaskStatus.RanToCompletion)
                {
                    return false;
                }
                if(this._circle != null && this._circle.Count > 0)
                {
                    return false;
                }
                return this._queue.Count == 0;
            }
        }

        public AsyncEvent()
        {
            this._queue = new ConcurrentQueue<Action>();
            this._circle = new Circle<AsyncEvent>();
            this.BufferLength = 10;
            this.Last = null;
            this.Next = null;
        }

        public void setFunc(Action<AsyncEvent> act)
        {
            this._func = act;
        }

        public void addChild(AsyncEvent childEvent)
        {
            this._circle.Add(childEvent);
        }

        public void SendCallBack(Action action)
        {
            while (this._queue.Count >= this.BufferLength) { };
            this._queue.Enqueue(action);
        }

        public Action PopCallBack()
        {
            Action act;
            if(this._queue.TryDequeue(out act))
            {
                return act;
            }
            else
            {
                return null;
            }
        }

        public void Start()
        {
            if (this._func != null)
            {
                this._task = new Task(() => this._func.Invoke(this));
                this._task.Start();
            }
            foreach(AsyncEvent e in this._circle)
            {
                e.Start();
            }
            Task childTask = new Task(() =>
            {
                while (this._circle.Count > 0)
                {
                    AsyncEvent current = this._circle.Current;
                    Action act = current.PopCallBack();
                    if (act != null)
                    {
                        this._queue.Enqueue(act);
                        this._circle.Next();
                    }
                    else
                    {
                        if (current.IsCompleted)
                        {
                            this._circle.Remove(current);
                        }
                    }
                }
            });
            childTask.Start();
        }

    }

    public class Circle<T> : IEnumerable where T : class, INode
    {
        private T _current;
        private T _head;
        private int _count;

        public T Head
        {
            get { return this._head; }
        }

        public int Count
        {
            get { return this._count; }
        }

        public T Current
        {
            get
            {
                if (this._current == null)
                {
                    this._current = this._head;
                }
                return this._current;
            }
        }

        public void Next()
        {
            this._current = (T)this._current.Next;
        }

        public Circle()
        {
            this._current = null;
            this._head = null;
            this._count = 0;
        }

        public void Add(T node)
        {
            if(this._head == null)
            {
                this._head = node;
                node.Last = node;
                node.Next = node;
            }
            else
            {
                T tail = (T)this._head.Last;
                this._head.Last = node;
                node.Next = this._head;
                node.Last = tail;
                tail.Next = node;              
            }
            this._count++;
        }

        public void Remove(T node)
        {
            node.Last.Next = node.Next;
            node.Next.Last = node.Last;
            if(_head == node)
            {
                this._head = (T)node.Next;
            }
            if(_current == node)
            {
                this._current = (T)node.Next;
            }
            node = null;
            this._count--;
        }

        public IEnumerator GetEnumerator()
        {
            return new CirclEnumerator<T>(this);
        }

        private class CirclEnumerator<T2> : IEnumerator where T2 : class, INode
        {
            private Circle<T2> _circle;

            public CirclEnumerator(Circle<T2> circle)
            {
                _circle = circle;
            }

            public object Current
            {
                get
                {
                    return _circle._current;
                }
            }

            public bool MoveNext()
            {
                if(_circle.Count <= 0)
                {
                    return false;
                }
                if(_circle._current == _circle._head.Last)
                {
                    _circle._current = _circle._head;
                    return false;
                }
                else
                {
                    if (_circle._current == null)
                    {
                        _circle._current = _circle._head;
                    }
                    else
                    {
                        _circle._current = (T2)_circle._current.Next;
                    }
                    return true;
                }
            }

            public void Reset()
            {
                _circle._head = null;
                _circle._current = null;
                _circle._count = 0;
            }
        }
    }

    public interface INode
    {
        INode Last { get; set; }
        INode Next { get; set; }
    }
}
