﻿using System;
using AsyncHelp;
using System.Threading;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            AsyncEvent mainEvent = new AsyncEvent();
            AsyncEvent e1 = new AsyncEvent();
            AsyncEvent e2 = new AsyncEvent();
            AsyncEvent e3 = new AsyncEvent();
            e1.setFunc((e) =>
            {
                Go4(1, 10, e);
            });
            e2.setFunc((e) =>
            {
                Go4(2, 10, e);
            });
            e3.setFunc((e) =>
            {
                Go4(3, 10, e);
            });
            mainEvent.addChild(e1);
            mainEvent.addChild(e2);
            mainEvent.addChild(e3);
            mainEvent.Start();
            while(!mainEvent.IsCompleted)
            {
                Action act = mainEvent.PopCallBack();
                if(act != null)
                {
                    act.Invoke();
                }
            }
            Console.ReadLine();
        }

        static string Go(object n)
        {
            int m = (int)n;
            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine("任务{0}正在执行：{1}", m, i);
            }
            return string.Format("任务{0}完成", n);
        }

        static string Go2(object n)
        {
            Thread.Sleep(2000);
            return string.Format("任务{0}完成", n);
        }

        static void Print(object n)
        {
            Console.WriteLine(n.ToString());
        }

        static string Go3(object n, AsyncIterator it)
        {
            int m = (int)n;
            int sum = 0;
            Random rd = new Random();
            for(int i = 0; i < m; i++)
            {
                int x = rd.Next(100, 1000);
                Thread.Sleep(x);
                Console.WriteLine("迭代进行中：{0}", i);
                sum += i;
                it.SendCallback((o => 
                {
                    Thread.Sleep(500);
                    Console.WriteLine("执行回调：{0}", o.ToString());
                    }), i);
            }
            return sum.ToString();
        }

        static void Go4(int id, int n, AsyncEvent e)
        {
            Random rd = new Random();
            for (int i = 0; i < n; i++)
            {
                //int x = rd.Next(100, 1000);
                Thread.Sleep(1000);
                Console.WriteLine("事件{0}正在执行：{1}", id, i);
                e.SendCallBack(() =>
                {
                    Thread.Sleep(500);
                    Console.WriteLine("执行回调：{0}", id);
                });
            }
        }
    }
}
